# API de Geração de assinatura PDF com certificado em nuvem PSC

Este é exemplo backend de integração dos serviços da API de assinatura com clientes baseados em tecnologia Java, utilizando certificado em nuvem PSC. É requisito que a aplicaçção seja WEB, pois a autorização de uso da chave será realizada mediante o direcionamento para a página do PSC para que o titular do certificado se autentique e autorize o uso do certificado.

A integração com um PSC homologado na ICP Brasil envolve três etapas principais.

1 - Geração de credenciais com PSC

O integrador deve entrar em contato com o PSC que deseja integrar para obter as credenciais (client_id e client_secret) com as quais será possível realizar as chamadas necessarias para comunicação com o respectivo serviço.

2 - Autenticação do titular do certificado

A aplicação WEB deverá direcionar o usário para a página de autenticação e autorização do certificado fornecida pelo PSC. A autenticação será realizada através da leitura de um QR Code ou outro modo oferecido pelo PSC. Após autenticação, o usuário será direcionado de volta para a aplicação de origem.

3 - Geração da assinatura

Com a etapa 2, será retornado um token que deverá ser repassado na chamada de assinatura no HUB Signer. Com este token, a assinatura será iniciada e finalizada utilizando a chave do usuário que está no respectivo PSC.


Este exemplo gera um token de acesso de acordo com [RFC 6749](https://datatracker.ietf.org/doc/html/rfc6749) e o utiliza para realizar assinatura PDF;

### Tech

O exemplo utiliza das bibliotecas Java abaixo:
* [SpringBoot Web] - Spring RestTemplate for create requests
* [JDK 8] - Java 8

### Variáveis que devem ser configuradas
O exemplo necessita ser configurado com credenciais de acesso válidas. É necessário configurar credenciais de acesso para API de assinatura e credenciais de acesso para serviço de Certificado em Nuvem.

As credenciais de acesso a API de assinatura podem ser obtidas através da documentação disponibilizada no [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário no [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não esteja cadastrado, [cadastra-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente à produção da assinatura.

| Variável            | Descrição                                | Local da Configuração |
|---------------------|------------------------------------------|-----------------------|
| <HUB_URL>           | Url base para API de assinatura.         | APIConfiguration.java |
| <HUB_AUTHORIZATION> | Token JWT de acesso a API de assinatura. | APIConfiguration.java |

| Variável            | Descrição                                                                                                                               | Local da Configuração |
|---------------------|-----------------------------------------------------------------------------------------------------------------------------------------|-----------------------|
| <PSC_URL>           | Url base para acesso ao PSC.                                                                                                            | PSCConfiguration.java |
| <REDIRECT_URL>      | Url de redirecionamento utilizada pelo PSC. O exemplo realiza tratamento de redirecionamentos na rota `/psc`                            | PSCConfiguration.java |
| <PSC_CLIENT_ID>     | Client ID para autenticação no PSC.                                                                                                     | PSCConfiguration.java |
| <PSC_CLIENT_SECRET> | Client Secret para autenticação no PSC.                                                                                                 | PSCConfiguration.java |
| <PSC_SCOPE>         | Escopo para assinaturas realizadas utilizando token obtido no PSC. Para este exemplo, utilize `single_signature` ou `signature_session` | PSCConfiguration.java |

### Uso

Para execução da aplicação de exemplo, compile diretamente as classes ou importe o projeto em sua IDE de preferência.
Utilizamos o JRE versão 8 para desenvolvimento e execução.

Acesse a página `/index.html` para exibir a página inicial de exemplo. Para realizar a assinatura PDF, é necessário autorizar o uso do certificado gerando um token de acesso, utilizando botão `Gerar Token`;

[SpringBoot Web]: <https://spring.io/>
[JDK 8]: <https://www.oracle.com/java/technologies/javase/javase-jdk8-downloads.html>
