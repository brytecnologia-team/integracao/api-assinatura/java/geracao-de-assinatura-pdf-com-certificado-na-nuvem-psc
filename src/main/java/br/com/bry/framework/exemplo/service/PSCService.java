package br.com.bry.framework.exemplo.service;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import br.com.bry.framework.exemplo.configuration.APIConfiguration;
import br.com.bry.framework.exemplo.configuration.PSCConfiguration;
import br.com.bry.framework.exemplo.dto.PSCAccessToken;

@Service
public class PSCService {
	Random random = new Random(System.currentTimeMillis());
	Map<String, String> memory = new HashMap<>();
	
	@Autowired
	private RestTemplate restTemplate;
	
	public String accessCodeUri(String id, String state) throws NoSuchAlgorithmException {
		String challenge = newCodeChallenge(id);
		
		StringBuilder uriBuilder = new StringBuilder(PSCConfiguration.URL);
		uriBuilder.append("/v0/oauth/authorize")
			.append("?response_type=code")
			.append("&code_challenge_method=S256")
			.append("&state=").append(state)
			.append("&client_id=").append(PSCConfiguration.CLIENT_ID)
			.append("&redirect_uri=").append(PSCConfiguration.REDIRECT_URL)
			.append("&scope=").append(PSCConfiguration.SCOPE)
			.append("&code_challenge=").append(challenge);
		
		return uriBuilder.toString();
	}
	
	public String accessToken(String id, String code) {
		String codeVerifier = memory.get(id);
		if (codeVerifier == null) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Code verifier não encontrado para usuário");
		}
		
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> body = new LinkedMultiValueMap<>();
        body.add("grant_type", "authorization_code");
        body.add("client_id", PSCConfiguration.CLIENT_ID);
        body.add("client_secret", PSCConfiguration.CLIENT_SECRET);
        body.add("redirect_uri", PSCConfiguration.REDIRECT_URL);
        body.add("code", code);
        body.add("code_verifier", codeVerifier);

		HttpEntity<?> apiRequest = new HttpEntity<>(body, headers);
		PSCAccessToken response;
		try {
			response = restTemplate.exchange(PSCConfiguration.URL + "/v0/oauth/token"
					, HttpMethod.POST, apiRequest, PSCAccessToken.class).getBody();
		} catch (RestClientException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());				
		}
		if (response == null || response.accessToken == null) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Invalid PSC response");			
		}
		return response.accessToken;
	}
	
	private String newCodeChallenge(String id) throws NoSuchAlgorithmException {
		byte[] randomBytes = new byte[32];
		random.nextBytes(randomBytes);
		byte[] codeVerifier = Base64.getUrlEncoder().withoutPadding().encode(randomBytes);
		memory.put(id, new String(codeVerifier));
		
		MessageDigest digest = MessageDigest.getInstance("SHA-256");
		byte[] challengeBytes = digest.digest(codeVerifier);
		return new String(Base64.getUrlEncoder().withoutPadding().encode(challengeBytes));
	}
}
