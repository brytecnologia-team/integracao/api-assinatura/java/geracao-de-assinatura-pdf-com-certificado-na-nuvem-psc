package br.com.bry.framework.exemplo.controller;

import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

import br.com.bry.framework.exemplo.mapper.PSCMapper;
import br.com.bry.framework.exemplo.service.PSCService;

@RestController
@CrossOrigin
public class PSCController {

	@Autowired
	private PSCService pscService;

	@GetMapping(value = "/code")
	public RedirectView getCode(HttpServletRequest request, HttpServletResponse response) throws NoSuchAlgorithmException {
		System.out.println("Requisição para geração de código de acesso recebida no backend");
		String id = PSCMapper.getID(request);
		String state = PSCMapper.getState(request);
		
		String url = pscService.accessCodeUri(id, state);
		return new RedirectView(url);
	}
	
	@GetMapping(value = "/psc")
	public String createToken(HttpServletRequest request, HttpServletResponse response, 
			@RequestParam(value="code", required=true) String code,
			@RequestParam(value="state", required=true) String state) throws Exception {
		System.out.println("Requisição para geração de token JWT recebida no backend");
		String id = PSCMapper.getID(state);
		
		return pscService.accessToken(id,  code);
	}
	
	
}
