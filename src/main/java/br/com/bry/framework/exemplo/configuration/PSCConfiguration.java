package br.com.bry.framework.exemplo.configuration;

public class PSCConfiguration {

	// URL Base para acesso ao PSC
	public static final String URL = "<PSC_URL>";
	
	// Redirect URL após autenticação no PSC
	public static final String REDIRECT_URL = "<REDIRECT_URL>";
	
	// Credencial client ID do PSC 
	public static final String CLIENT_ID = "<PSC_CLIENT_ID>";
	
	// Credencial client Secret do PSC
	public static final String CLIENT_SECRET = "<PSC_CLIENT_SECRET>";
	
	// Scope do token de assinaturas do PSC. Valores disponíveis: single_signature | multi_signature | signature_session
	public static final String SCOPE = "<PSC_SCOPE>";
}
