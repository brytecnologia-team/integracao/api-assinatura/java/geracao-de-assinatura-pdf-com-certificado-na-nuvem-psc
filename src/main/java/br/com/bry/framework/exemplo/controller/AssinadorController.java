package br.com.bry.framework.exemplo.controller;

import java.io.InputStream;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.bry.framework.exemplo.dto.AssinaturaRequest;
import br.com.bry.framework.exemplo.mapper.Assinatura;
import br.com.bry.framework.exemplo.service.AssinadorService;

@RestController
@CrossOrigin
public class AssinadorController {

	@Autowired
	private AssinadorService assinadorService;

	@PostMapping(value = "/sign", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<?> initializeSignature(HttpServletRequest request, HttpServletResponse response) throws Exception {
		System.out.println("Requisição de assinatura recebida no backend");
		
		AssinaturaRequest assinaturaRequest = Assinatura.GetRequest(request);
		InputStream documentoAssinado = assinadorService.assinar(assinaturaRequest);
		
		Resource resource = new InputStreamResource(documentoAssinado);
		return ResponseEntity.status(HttpStatus.OK)
			.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"signed.pdf\"")
			.contentType(MediaType.APPLICATION_PDF)
			.body(resource);
	}


}
