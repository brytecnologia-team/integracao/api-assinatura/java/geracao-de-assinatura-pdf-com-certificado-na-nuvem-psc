package br.com.bry.framework.exemplo.mapper;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.support.StandardMultipartHttpServletRequest;
import org.springframework.web.server.ResponseStatusException;

import br.com.bry.framework.exemplo.configuration.SignatureConfiguration;
import br.com.bry.framework.exemplo.dto.AssinaturaRequest;

public class Assinatura {
	private Assinatura() {}
	
	public static AssinaturaRequest GetRequest(HttpServletRequest request) throws ResponseStatusException {
		AssinaturaRequest res = new AssinaturaRequest();
		res.perfil = SignatureConfiguration.PERFIL;
		res.algoritmoHash = SignatureConfiguration.ALGORITMO_HASH;
		res.razao = SignatureConfiguration.RAZAO;
		res.contato = SignatureConfiguration.CONTATO;
		res.local = SignatureConfiguration.LOCAL;
		res.tipoRestricao = SignatureConfiguration.TIPO_RESTRICAO;
		
		List<MultipartFile> images= ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("imagem");
		if (images != null && !images.isEmpty()) {
			try {
				Resource resource = images.get(0).getResource();
				if (resource.contentLength() > 0) {
					res.imagem = resource;					
				}
			} catch (Exception e) {
				// Ignore exception
			}
			if (res.imagem != null) {
				res.posicao = request.getParameter("posicao");
				res.coordenadaInicialX = request.getParameter("coordenadaInicialX");
				res.coordenadaInicialY = request.getParameter("coordenadaInicialY");
				
				res.altura = request.getParameter("altura");
				if (res.altura == null) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parâmetro 'altura' é obrigatório para requisições com imagem");
				}
				res.largura = request.getParameter("largura");
				if (res.largura == null) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parâmetro 'largura' é obrigatório para requisições com imagem");
				}
				res.pagina = request.getParameter("pagina");
				if (res.pagina == null) {
					throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parâmetro 'pagina' é obrigatório para requisições com imagem");
				}
			}
		}
		
		res.token = request.getParameter("token");
		if (res.token == null) {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parâmetro 'token' é obrigatório");		
		}
		List<MultipartFile> documents = ((StandardMultipartHttpServletRequest) request).getMultiFileMap()
				.get("documento");
		if (documents != null && !documents.isEmpty()) {
			res.documento = documents.get(0).getResource();
		} else {
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Parâmetro 'documento' é obrigatório");			
		}

		return res;
	}
}
