package br.com.bry.framework.exemplo.dto;

import org.springframework.core.io.Resource;

public class AssinaturaRequest {
	public String perfil;
	public String algoritmoHash;
	public String razao;
	public String contato;
	public String local;
	public String tipoRestricao;
	
	public Resource imagem;
	public String altura;
	public String largura;
	public String coordenadaInicialX;
	public String coordenadaInicialY;
	public String posicao;
	public String pagina;

	public Resource documento;
	public String token;
}
