package br.com.bry.framework.exemplo.configuration;

public class SignatureConfiguration {
	// Perfil da assinatura que será gerada
	public static final String PERFIL = "TIMESTAMP";
	
	// Algoritmo de hash utilizado na assinatura
	public static final String ALGORITMO_HASH = "SHA256";
	
	// Razao da assinatura. Opcional
	public static final String RAZAO = null;
	
	// Contato informado na assinatura. Opcional
	public static final String CONTATO = null;

	// Local informado na assinatura. Opcional
	public static final String LOCAL= null;
	
	// Tipo de restrição da assinatura. Opcional.
	public static final String TIPO_RESTRICAO = null;
}
