package br.com.bry.framework.exemplo.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PSCAccessToken {
    @JsonProperty("access_token")
	public String accessToken;
}
