package br.com.bry.framework.exemplo.service;

import java.io.ByteArrayInputStream;
import java.io.InputStream;

import org.apache.tomcat.util.codec.binary.Base64;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ResponseStatusException;

import br.com.bry.framework.exemplo.configuration.APIConfiguration;
import br.com.bry.framework.exemplo.configuration.PSCConfiguration;
import br.com.bry.framework.exemplo.dto.AssinaturaRequest;

@Service
public class AssinadorService {
	@Autowired
	private RestTemplate restTemplate;

	public InputStream assinar(AssinaturaRequest request) throws ResponseStatusException {
		MultiValueMap<String, Object> body;
		try {
			body = this.getBody(request);
		} catch ( JSONException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());			
		}
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Authorization", "Bearer " + APIConfiguration.AUTHORIZATION);
		headers.set("kms_type", "PSC");
		headers.setContentType(MediaType.MULTIPART_FORM_DATA);
		
		HttpEntity<?> apiRequest = new HttpEntity<>(body, headers);
		String[] response;
		try {
			response = restTemplate.exchange(APIConfiguration.URL + "/fw/v1/pdf/kms/lote/assinaturas"
					, HttpMethod.POST, apiRequest, String[].class).getBody();
		} catch (RestClientException e) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());				
		}
		if (response == null || response.length <= 0) {
			throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, "Invalid API response");
		}
		return new ByteArrayInputStream(Base64.decodeBase64(response[0]));
	}
	
	private MultiValueMap<String, Object> getBody(AssinaturaRequest request) throws JSONException {
		MultiValueMap<String, Object> map = new LinkedMultiValueMap<>();

		Object documento = request.documento;
		map.add("documento", documento);

		JSONObject dadosAssinatura = new JSONObject();
		JSONObject kmsData = new JSONObject();
		kmsData.put("url", PSCConfiguration.URL);
		kmsData.put("token", request.token);
		dadosAssinatura.put("kms_data", kmsData);
		dadosAssinatura.put("perfil", request.perfil);
		dadosAssinatura.put("algoritmoHash", request.algoritmoHash);
		if (request.razao != null) {
			dadosAssinatura.put("razao", request.razao);			
		}
		if (request.contato != null) {
			dadosAssinatura.put("contato", request.contato);			
		}
		if (request.local != null) {
			dadosAssinatura.put("local", request.local);			
		}
		if (request.tipoRestricao != null) {
			dadosAssinatura.put("tipoRestricao", request.tipoRestricao);			
		}
		dadosAssinatura.put("tipoRetorno", "BASE64");
		map.add("dados_assinatura", dadosAssinatura.toString());
		
		if (request.imagem != null) {
			JSONObject configuracaoImagem = new JSONObject();
			configuracaoImagem.put("altura", request.altura);
			configuracaoImagem.put("largura", request.largura);
			configuracaoImagem.put("coordenadaX", request.coordenadaInicialX);
			configuracaoImagem.put("coordenadaY", request.coordenadaInicialY);
			configuracaoImagem.put("posicao", request.posicao);
			configuracaoImagem.put("pagina", request.pagina);
			
			map.add("configuracao_imagem", configuracaoImagem.toString());
			map.add("imagem", request.imagem);
		}
		
		return map;
	}
}
